class Group {
    var name : String = ""
    var members : [String] = []
    
    init(name:String) {
        self.name = name
    }
    // Keep sorted list of members
    func addMember(_ member: String) {
        members.append(member)
        members.sort(by: { $0 &lt; $1
        })
    }
}

class Member {
    var name : String = ""
    var noGroups : [String] = []
    
    init(name: String, noGroups : [String]) {
        self.name = name
        self.noGroups = noGroups
    }
    
    func canJoinGroup(name groupName: String) -&gt; Bool {
        return !noGroups.contains(groupName)
    }
}

show("This application tries to find best group for each member.")
// Ask group names
let lines = ask("Paste groups and members")

var groups : [Group] = []
var members : [Member] = []

// Separate lines
var lineNum = 0
for line in lines.components(separatedBy: "\n") {
    var lineFields = line.components(separatedBy: ",")
    if lineNum == 0 {
        // Create Group objects from names
        for field in lineFields {
            var groupName = field.trimmingCharacters(in: CharacterSet(charactersIn: " "))
            groups.append(Group(name:groupName))
        }
    } else {
        // Parse for members
        var i = 0
        var memberName = ""
        var noGroups : [String] = []
        for field in lineFields {
            var trimmedString = field.trimmingCharacters(in: CharacterSet(charactersIn: " "))
            if i == 0 {
                memberName = trimmedString
            } else {
                let groupIndex:Int? = Int(trimmedString)
                if groupIndex != nil &amp;&amp; groupIndex! &gt; 0 &amp;&amp; groupIndex! &lt;= groups.count {
                    noGroups.append(groups[groupIndex! - 1].name)
                }
            }
            i += 1
        }
        members.append(Member(name: memberName, noGroups: noGroups))
    }
    lineNum += 1
}
// Sort members so that those members with fewer group options are handled first
members.sort(by: { (groups.count - $0.noGroups.count) &lt; (groups.count - $1.noGroups.count)
})

class FilledGroup {
    var groupIndex = 0
    var memberCount = 0
}
var filledGroups : [FilledGroup] = []
var gIndex = 0
for gIndex in 0..&lt;groups.count {
    var filledGroup = FilledGroup()
    filledGroup.groupIndex = gIndex
    filledGroups.append(filledGroup)
}
// For each member select a group with least number of members
for member in members {
    // Sort groups by number of members
    filledGroups.sort(by: { $0.memberCount &lt; $1.memberCount
    })
    // Find a group that a member wants to join
    for filledGroup in filledGroups {
        if member.canJoinGroup(name: groups[filledGroup.groupIndex].name) {
            groups[filledGroup.groupIndex].addMember(member.name)
            filledGroup.memberCount += 1
            break
        }
    }
}

// Generate output
var result = ""
for group in groups {
    var groupRow = ""
    groupRow += group.name
    groupRow += ": "
    var i = 0
    for memberName in group.members {
        if i &gt; 0 {
            groupRow += ", "
        }
        groupRow += memberName
        i += 1
    }
    result += "\n"
    result += groupRow
}
show("Result:" + result)