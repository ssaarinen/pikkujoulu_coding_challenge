package com.remion.sample;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

public class InputParser {

	private List<Group> groups;
	private List<Entry> entries;
	
	
	public InputParser() {
		
	}
	
	public boolean readInput(BufferedReader in) {
        try {
            
            while (true) {
                String input = in.readLine();

                if ("".equals(input) || input == null) {
                    break;
                }

                String[] parts = input.trim().split(",");
                
                if (groups == null) {
                	groups = new ArrayList<Group>();
                	for (String part : parts) {
                		Group gp = new Group(part.trim());
                		if (groups.contains(gp)) {
                			throw new IOException("Same group '" + gp.getName() + "' cannot be added multiple times!");
                		} else {
                			groups.add(gp);
                		}
                	}
                } else {
                	if (entries == null) {
                		entries = new ArrayList<Entry>();
                	}
                	
                	String entryName = null;
                	List<Group> forbiddenGroups = new ArrayList<Group>();
                	for (int i = 0; i < parts.length; ++i) {
                		if (i == 0) {
                			entryName = parts[i].trim();
                		} else {
                			int index = Integer.parseInt(parts[i].trim()) - 1;
                			if (index < 0 || index >= groups.size()) {
                				throw new IOException("Invalid group index '" + (index + 1) + "' for entry '" + entryName + "'");
                			} else {
                				forbiddenGroups.add(groups.get(index));
                			}
                		}
                	}
                	
                	List<Group> possibleGroups = new ArrayList<Group>();
                	for (Group group : groups) {
                		if (!forbiddenGroups.contains(group)) {
                			possibleGroups.add(group);
                		}
                	}
                	
                	if (possibleGroups.size() > 0) {
	                	Entry entry = new Entry(entryName,forbiddenGroups,possibleGroups);
	                	if (entries.contains(entry)) {
	                		throw new IOException("Same entry '" + entry.getName() + "' cannot be added multiple times!");
	                	} else {
	                		entries.add(entry);
	                	}
                	} else {
                		// Skip those entries which don't want to belong to any group
                	}
                }
            }
            
            if (groups != null) {
            	if (entries == null) {
            		entries = new ArrayList<Entry>();
            	}
            	return true;
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        
        return false;
	}
	
	public List<Group> getGroups() {
		return groups;
	}
	
	public List<Entry> getEntries() {
		return entries;
	}
}
