package com.remion.sample;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class SolutionInfo {
	
	private List<Entry> entries;
	private List<Group> groups;
	private short usedAlgorithm;
	private int idealEntryCountPerGroup;
	private int incompleteGroupCount;
	private long possibleResultCount;
	
	private Map<Group,Integer> minEntriesForGroup;
	private Map<Group,Integer> maxEntriesForGroup;
	
	int entriesWithoutRestrictions;
	int groupsWithoutRestrictions;
	private int idealEntryCountPerGroupWithoutRestrictions;
	private int incompleteGroupCountWithoutRestrictions;
	
	public SolutionInfo(List<Entry> entries, List<Group> groups,short usedAlgorithm) {
		this.entries = entries;
		this.groups = groups;
		this.usedAlgorithm = usedAlgorithm;
		minEntriesForGroup = new TreeMap<Group,Integer>();
		maxEntriesForGroup = new TreeMap<Group,Integer>();
		if (groups.size() > 0) {
			idealEntryCountPerGroup = entries.size() / groups.size();
			incompleteGroupCount = 0;
			if (entries.size() % groups.size() > 0) {
				idealEntryCountPerGroup++;
				
				incompleteGroupCount = groups.size() - (entries.size() % groups.size());
			}
	
			possibleResultCount = getPossibleResultCountInternal();	
			entriesWithoutRestrictions = entries.size();
			groupsWithoutRestrictions = groups.size();
			idealEntryCountPerGroupWithoutRestrictions = idealEntryCountPerGroup;
			incompleteGroupCountWithoutRestrictions = incompleteGroupCount;
			
			findMinAndMaxEntriesForGroups();
		}
	}
	
	public List<Entry> getEntries() {
		return entries;
	}
	
	public List<Group> getGroups() {
		return groups;
	}
	
	public short getUsedAlgorithm() {
		return usedAlgorithm;
	}
	
	public int getIdealEntryCountPerGroup() {
		return idealEntryCountPerGroup;
	}
	
	public int getIncompleteGroupCount() {
		return incompleteGroupCount;
	}
	
	public int getEntriesWithoutRestrictions() {
		return entriesWithoutRestrictions;
	}
	
	public int getGroupsWithoutRestrictions() {
		return groupsWithoutRestrictions;
	}
	
	public int getIdealEntryCountPerGroupWithoutRestrictions() {
		return idealEntryCountPerGroupWithoutRestrictions;
	}
	
	public int getIncompleteGroupCountWithoutRestrictions() {
		return incompleteGroupCountWithoutRestrictions;
	}
	
	public long getPossibleResultCount() {
		return possibleResultCount;
	}
	
	public int getMinEntryCountForGroup(Group group) {
		return minEntriesForGroup.get(group);
	}
	
	public int getMaxEntryCountForGroup(Group group) {
		return maxEntriesForGroup.get(group);
	}
	
	private long getPossibleResultCountInternal() {
		long resultCount = 0;
		for (Entry entry : entries) {
			if (entry.getPossibleGroups().size() > 0) {
				if (resultCount == 0) {
					resultCount = entry.getPossibleGroups().size();
				} else {
					resultCount = resultCount * entry.getPossibleGroups().size();
				}
			}
		}
		return resultCount;
	}
	
	private void findMinAndMaxEntriesForGroups() {
		
		for (Group group : groups) {
			int minEntries = 0;
			int maxEntries = 0;
			for (Entry entry : entries) {
				if (entry.getPossibleGroups().contains(group)) {
					maxEntries++;
					
					if (entry.getPossibleGroups().size() == 1) {
						minEntries++;
					}
				}
			}
			
			if (idealEntryCountPerGroup > maxEntries) {
				entriesWithoutRestrictions -= maxEntries;
				groupsWithoutRestrictions--;
			} else if (idealEntryCountPerGroup < minEntries) {
				entriesWithoutRestrictions -= minEntries;
				groupsWithoutRestrictions--;
			}
			
			minEntriesForGroup.put(group, new Integer(minEntries));
			maxEntriesForGroup.put(group, new Integer(maxEntries));
		}
		
		if (entriesWithoutRestrictions != entries.size() || groupsWithoutRestrictions != groups.size()) {
			incompleteGroupCountWithoutRestrictions = 0;
			idealEntryCountPerGroupWithoutRestrictions = 0;
			if (groupsWithoutRestrictions > 0) {
				idealEntryCountPerGroupWithoutRestrictions = entriesWithoutRestrictions / groupsWithoutRestrictions;
			
				if (entriesWithoutRestrictions % groupsWithoutRestrictions > 0) {
					idealEntryCountPerGroupWithoutRestrictions++;
					
					incompleteGroupCountWithoutRestrictions = groupsWithoutRestrictions - (entriesWithoutRestrictions % groupsWithoutRestrictions);
				}
			}
			
		}
	}
	
}
