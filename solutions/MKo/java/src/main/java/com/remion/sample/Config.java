package com.remion.sample;

public class Config {
	public static boolean IS_DEBUG_ENABLED = false;
	
	public static final short ALGORITHM_BRUTE_FORCE = 1;
	public static final short ALGORITHM_QUICK       = 2;
	public static final short ALGORITHM_IDEAL       = 3;
	
	public static String algorithmToString(short usedAlgorithm) {
		switch (usedAlgorithm) {
		case ALGORITHM_BRUTE_FORCE: return "brute-force";
		case ALGORITHM_QUICK: return "quick";
		case ALGORITHM_IDEAL: return "ideal";
		default: return "unknown";
		}
	}
}
