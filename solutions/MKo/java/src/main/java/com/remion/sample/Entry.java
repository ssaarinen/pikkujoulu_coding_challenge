package com.remion.sample;

import java.util.List;

public class Entry implements Comparable<Entry> {

	private String name;
	private List<Group> forbiddenGroups;
	private List<Group> possibleGroups;
	
	public Entry(String name, List<Group> forbiddenGroups, List<Group> possibleGroups) {
		this.name = name;
		this.forbiddenGroups = forbiddenGroups;
		this.setPossibleGroups(possibleGroups);
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<Group> getForbiddenGroups() {
		return forbiddenGroups;
	}
	public void setForbiddenGroups(List<Group> forbiddenGroups) {
		this.forbiddenGroups = forbiddenGroups;
	}

	@Override
	public int compareTo(Entry o) {
	   return this.name.compareTo(o.name);
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Entry other = (Entry) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	public List<Group> getPossibleGroups() {
		return possibleGroups;
	}

	public void setPossibleGroups(List<Group> possibleGroups) {
		this.possibleGroups = possibleGroups;
	}
	
}
