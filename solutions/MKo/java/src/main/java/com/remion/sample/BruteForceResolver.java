package com.remion.sample;

import java.util.ArrayList;
import java.util.List;

public class BruteForceResolver {
	private SolutionInfo info;
	private List<Entry> entries;
	private List<Group> groups;
	
	public BruteForceResolver(SolutionInfo info) {
		this.info = info;
		this.entries = info.getEntries();
		this.groups = info.getGroups();
	}
	
	public Result findResult() {
		List<Result> results = findPossibleResults(0);
		Result result = findBestResult(results);
		if (result != null) {
			return result;
		} else {
			// Resolving failed... return empty result
			if (Config.IS_DEBUG_ENABLED) {
				System.out.println("Failed to find result using brute force algorithm!!");
			}
			return new Result(groups);
		}
	}
	
	
	private List<Result> findPossibleResults(int entryIndex) {
		if (entryIndex == entries.size() - 1) {
			List<Result> results = new ArrayList<Result>();
			for (Group group : entries.get(entryIndex).getPossibleGroups()) {
				Result result = new Result(groups);
				result.addEntry(group, entries.get(entryIndex));
				results.add(result);
			}
			return results;
		} else {
			List<Result> results = new ArrayList<Result>();
			for (Group group : entries.get(entryIndex).getPossibleGroups()) {
				List<Result> tmp = findPossibleResults(entryIndex + 1);
				for (Result result : tmp) {
					result.addEntry(group, entries.get(entryIndex));
				}
				results.addAll(tmp);
			}
			return results;
		}
	}

	private Result findBestResult(List<Result> results) {
		int minIdealness = Integer.MAX_VALUE;
		Result bestResult = null;
		for (Result result : results) {
			int idealness = result.resultIdealness(info);
			if (idealness < minIdealness) {
				minIdealness = idealness;
				bestResult = result;
				if (idealness == 0) {
					break;
				}
			}
		}
		
		if (Config.IS_DEBUG_ENABLED) {
			System.out.println("Brute-force result idealness is " + minIdealness);
		}
		
		return bestResult;
	}
}
