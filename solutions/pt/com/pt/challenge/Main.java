// PT

package com.pt.challenge;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;

public class Main {

	public static void main(String[] args) throws IOException { 
		
		int bytes = System.in.available();
		if (bytes < 1) {
			System.out.println("No STDIN input!");
			return;
		}
		
		byte[] buf = new byte[bytes];
		System.in.read(buf, 0, bytes);
		String input = new String(buf, 0, bytes);
		
		process(input);
		 
	}
	
	
	static void process(String input) {
		
		// States: reading group names / parsing entries
		boolean isReadingGroupNames = true;
		
		// Storage for group names
		List<String> groupNames = new ArrayList<String>();

		// Keep track of excluded groups. Reset to false at every line. Size equal to group names
		boolean[] lineExclusions = null;
		
		// For each group (outer list), keep track of the found results ( inner List<String> )
		List<List<String>> resultList = new ArrayList<List<String>>();
		
		
		// Loop lines
		StringTokenizer lineTokenizer = new StringTokenizer(input, "\n");						
		while (lineTokenizer.hasMoreElements()) {
			
			String line = lineTokenizer.nextToken().trim();
			
			StringTokenizer commaTokenizer = new StringTokenizer(line, ",");
			
			String name = null;				
			
			// Loop commas
			while (commaTokenizer.hasMoreTokens()) {	

				String token = commaTokenizer.nextToken();

				// Parse group names
				if (isReadingGroupNames) {
					groupNames.add(token);	
				} 
				// Parse entries
				else {

					// First entry is the name
					if (name == null) {
						name = token;	
					} else {
						// Exclude group number
						int groupNumber = Integer.parseInt(token);
						if (groupNumber < 0 || groupNumber > groupNames.size()) throw new IllegalArgumentException("Invalid group number " + groupNumber);
						lineExclusions[ groupNumber - 1 ] = true;
					}
				}
			}

			// Line parsed, process results
			
			if (isReadingGroupNames) {
				
				// Switch from group reading to entry reading
				isReadingGroupNames = false;

				// Create lists for results
				for (int i = 0; i < groupNames.size(); i++) {
					resultList.add( new ArrayList<String>());
				}
				
			} else {				

				// Find best group for this name
				int minIndex = findBestGroup(lineExclusions, resultList);
				if (minIndex != Integer.MAX_VALUE) {
					List<String> list = resultList.get(minIndex);
					list.add(name);
				}
			}

			// Create / Reset exclusions 
			lineExclusions = new boolean[ groupNames.size() ];
			
		}
		
		printResults(groupNames, resultList);
	}


	// Scan shortest list from groups were NOT excluded
	private static int findBestGroup(boolean[] lineExclusions, List<List<String>> resultList) {
		int minSize = Integer.MAX_VALUE; 
		int minIndex = 0;				
		
		for (int i = 0; i < lineExclusions.length; i++) {
		
			// Is excluded?
			if (lineExclusions[i] == false) {
				List<String> list = resultList.get(i);
				if (list.size() < minSize) {
					minSize = list.size();
					minIndex = i;
				}
			}
		}
		return minIndex;
	}


	// Sort and print results to STDOUT
	private static void printResults(List<String> groupNames, List<List<String>> resultList) {
		Iterator<List<String>> resultListIterator = resultList.iterator();
		for (String groupName : groupNames) {
			
			List<String> list = resultListIterator.next();

			Collections.sort(list);
			
			StringBuffer names = generateCSV(list);			
			System.out.println(groupName + ": " + names.toString());
		}
	}


	// Convert list to CSV
	private static StringBuffer generateCSV(List<String> list) {
		StringBuffer names = new StringBuffer();
		for (String name : list) {
			if (names.length() > 0) names.append(',');
			names.append(name);
			
		}
		return names;
	}
}
