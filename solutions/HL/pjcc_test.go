package main

import (
	"testing"
	"strings"
	"bytes"
	"strconv"
	"math/rand"
	"time"
)

type TestData struct {
	input string
	output string
}

var result string

var testdata = []TestData {
	{   "A\n" +
	    "aaa\n" +
	    "bbb\n" +
	    "ccc\n",
	    
	    "A: aaa, bbb, ccc\n" },
	
	
	{   "A\n" +
	    "aaa,1\n" +
	    "bbb,1\n" +
	    "ccc,1\n",
	    
	    "A: \n" },
	
	
	{	"A,B,C\n" +
		"aaa,1,3\n" +
		"bbb,2,3\n" +
		"ccc,3\n" +
		"ddd,2\n" +
		"eee,1\n" +
		"fff,3\n" +
		"ggg,1\n" +
		"hhh\n" +
		"iii\n" +
		"jjj\n" +
		"kkk\n" +
		"lll,1,2\n" +
		"mmm,2\n\n\na",
		
		"A: bbb, ccc, fff, hhh\n" +
		"B: aaa, eee, ggg, iii\n" + 
		"C: ddd, jjj, kkk, lll, mmm\n" },
	
	
	{   "A,B,C\n" +
		"Roope,2,3\n" + 
		"Aku,1,2\n" +
		"Mikki\n" + 
		"Hessu,3\n",
		
		"A: Roope\n" + 
		"B: Hessu\n" + 
		"C: Aku, Mikki\n" },
	
	
	{   "A,B,C,D\n" + 
		"aaa,1,2,3,4\n" + 
		"bbb,2,3,4\n" +
		"ccc,2,3,4\n" +
		"ddd\n" +
		"eee,1,2,3,4\n" +
		"fff,2,3,4\n" ,
	
	    "A: bbb, ccc, fff\n" +
		"B: ddd\n" +
		"C: \n" +
		"D: \n" }, 
}


func TestPjcc(t *testing.T) {
	for _, td := range testdata {
		r := strings.NewReader(td.input)
		buf := new(bytes.Buffer)
		pjcc(r, buf)
		s := buf.String()
		if (s != td.output) {
			t.Errorf("input:\n%q\n\ngot:\n%q\n\nexpected:\n%q", td.input, s, td.output)
		}
	}
}

func benchmarkPjcc(td string, b *testing.B) {
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		r := strings.NewReader(td)
		buf := new(bytes.Buffer)
		pjcc(r, buf)
		result = buf.String()
	}
}

func BenchmarkPjcc10_10(b *testing.B) {
	benchmarkPjcc(getBenchmarkTestData(10, 10), b)
}

func BenchmarkPjcc10_100(b *testing.B) {
	benchmarkPjcc(getBenchmarkTestData(10, 100), b)
}

func BenchmarkPjcc10_1000(b *testing.B) {
	benchmarkPjcc(getBenchmarkTestData(10, 1000), b)
}

func BenchmarkPjcc10_10000(b *testing.B) {
	benchmarkPjcc(getBenchmarkTestData(10, 10000), b)
}

func BenchmarkPjcc100_10(b *testing.B) {
	benchmarkPjcc(getBenchmarkTestData(100, 10), b)
}

func BenchmarkPjcc100_100(b *testing.B) {
	benchmarkPjcc(getBenchmarkTestData(100, 100), b)
}

func BenchmarkPjcc100_1000(b *testing.B) {
	benchmarkPjcc(getBenchmarkTestData(100, 1000), b)
}

func BenchmarkPjcc100_10000(b *testing.B) {
	benchmarkPjcc(getBenchmarkTestData(100, 10000), b)
}

func BenchmarkPjcc1000_10(b *testing.B) {
	benchmarkPjcc(getBenchmarkTestData(1000, 10), b)
}

func BenchmarkPjcc1000_100(b *testing.B) {
	benchmarkPjcc(getBenchmarkTestData(1000, 100), b)
}

func BenchmarkPjcc1000_1000(b *testing.B) {
	benchmarkPjcc(getBenchmarkTestData(1000, 1000), b)
}

func BenchmarkPjcc1_10000(b *testing.B) {
	benchmarkPjcc(getBenchmarkTestData(1, 10000), b)
}

func getBenchmarkTestData(roomCount int, dudeCount int) string {
	var buffer bytes.Buffer
	buffer.WriteString("room0")
	for i := 1; i < roomCount; i++ {
		buffer.WriteString(",room" + strconv.Itoa(i))
	}
	buffer.WriteString("\n")
	
	r := rand.New(rand.NewSource(time.Now().UnixNano()))
	for i := 0; i < dudeCount; i++ {
		buffer.WriteString("dude" + strconv.Itoa(i))
		nogoRoomsCount := r.Intn(roomCount)
		nogoRooms := r.Perm(nogoRoomsCount)
		for _, v := range nogoRooms {
			buffer.WriteString("," + strconv.Itoa(v))
		}
		buffer.WriteString("\n")
	}
	
	return buffer.String()
}
