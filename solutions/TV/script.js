(function() {
    var teams;
    var people;
    var notInAnyTeam;
    
    document.addEventListener("DOMContentLoaded", function() {
        document.getElementById("inputFile").addEventListener("change", handleFileEvent);
    });

    function handleFileEvent(evt) {
        var files = evt.target.files;
        if (files.length > 0) {
            var file = files[0];
            var reader = new FileReader();
            reader.onload = function() {
                handle(reader.result);
            };
            reader.readAsText(file);
        }
    }

    function handle(inputStr) {
        people = [];
        notInAnyTeam = [];
        
        var lineBreak = inputStr.charAt(inputStr.indexOf('\n') - 1) == '\r' ? '\r\n' : '\n';
        var lines = inputStr.split(lineBreak);
        teams = lines[0].split(',').map(function(name, index) {
            return {name: name, index: index, people: []};
        });
        
        for (var i = 1; i < lines.length; i++) {
            var person = lines[i].split(',');
            var excludedTeams = person.length > 1 ? person.slice(1, person.length) : [];
            var possibleTeams = getPossibleTeams(teams, person.length > 1 ? excludedTeams : []);
            people.push({
                name: person[0],
                possibleTeams: possibleTeams
            });
        }
        
        
        for (var i = 0; i < people.length; i++) {
            var person = people[i];
            if (person.possibleTeams.length == 0) {
                notInAnyTeam.push(person.name);
            } else {
                var team = getSmallestTeam(teams, person.possibleTeams);
                team.people.push(person.name);
            }
        }
        
        var result = teams.reduce(function(a, b) {
            return a + b.name + ": " + b.people.join(', ') + "\n";
        }, "");

        if (notInAnyTeam.length > 0) {
            var row = "Not in any team: " + notInAnyTeam.join(', ');
            result += row + '\n';
        }
        
        document.getElementById('result').value = result;
    }
    
    function getPossibleTeams(teams, excluded) {
        return teams.filter(function(element, index) {
            return !contains(excluded, index + 1 + '');
        }).map(function(element) {
            return element.index;
        }); 
    }
    
    function getSmallestTeam(teams, possbileIndeces) {
        var smallest;
        for (var i = 0; i < possbileIndeces.length; i++) {
            var team = teams[possbileIndeces[i]];
            if (team.people.length === 0) {
                return team;
            } else if (!smallest || smallest.people.length > team.people.length) {
                smallest = team;
            }
        }
        return smallest;
    }
    
    function contains(array, element) {
        for (var i = 0; i < array.length; i++) {
            if (array[i] == element) {
                return true;
            }
        }
        return false;
    }

})();